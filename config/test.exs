use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :strawlixir, Strawlixir.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :strawlixir, Strawlixir.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "root",
  database: "strawlixir_test",
  size: 1 # Use a single connection for transactional tests
