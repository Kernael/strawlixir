# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :strawlixir, Strawlixir.Endpoint,
  url: [host: "localhost"],
  root: Path.dirname(__DIR__),
  secret_key_base: "gzJ3IFNJjQT8BFEel7A8+ZM+uNBIAn6Sg+4nY43chJ6RlnA9ZHKJIk/oU7hH1Af3",
  debug_errors: false,
  pubsub: [name: Strawlixir.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures app Repo
config :strawlixir, Strawlixir.Repo,
  pool: Ecto.Adapters.SQL.Sandbox

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
