defmodule Strawlixir.Router do
  use Strawlixir.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Strawlixir do
    pipe_through :browser # Use the default browser stack

    get "/",            PollController, :new
    resources "/polls", PollController, except: [:edit, :update]
  end

  # Other scopes may use custom stacks.
  # scope "/api", Strawlixir do
  #   pipe_through :api
  # end
end
